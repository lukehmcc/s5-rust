# s5-rust

This is a minimal implementation of redsolver's S5 node in rust. 
The purpose of this project is to take advantage of BLAKE3's superior implementation and maturity in rust over the dart implementation.